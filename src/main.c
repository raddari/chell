#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>


typedef struct command {
  int argc;
  char** argv;
} command_t;

static command_t parse_command(char* line);
static void free_argv(command_t* command);

int main() {
  char* line = NULL;
  size_t len = 0;
  ssize_t nread = 0;

  while ((nread = getline(&line, &len, stdin)) != -1) {
    command_t cmd = parse_command(line);
    free_argv(&cmd);
  }

  free(line);
  return 0;
}

static command_t parse_command(char* line) {
  command_t cmd = { .argc = 1 };
  char* save = NULL;
  char* token = strtok_r(line, " ", &save);
  while ((token = strtok_r(NULL, " ", &save))) {
    ++cmd.argc;
  }

  // reserve 1 extra for a NULL terminated argv - exec expects this
  cmd.argv = malloc((cmd.argc + 1) * sizeof *cmd.argv);
  token = line;
  for (int i = 0; i < cmd.argc; ++i) {
    cmd.argv[i] = malloc(strlen(token) + 1);
    strcpy(cmd.argv[i], token);
    token = strchr(token, '\0') + 1;
  }
  cmd.argv[cmd.argc] = NULL;

  return cmd;
}

static void free_argv(command_t* command) {
  for (int i = 0; i < command->argc; ++i) {
    free(command->argv[i]);
    command->argv[i] = NULL;
  }
  free(command->argv);
  command->argv = NULL;
}
